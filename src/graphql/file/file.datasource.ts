import {Injectable, ProviderScope} from "@graphql-modules/di";
import {RESTDataSource} from "apollo-datasource-rest";
import * as process from "process";
import {TOKEN_HEADER} from "../../index";

@Injectable({
    scope: ProviderScope.Session
})
export default class FileDataSource extends RESTDataSource {


    constructor() {
        super();
        this.baseURL = process.env.URL_API_GATEWAY_FILE
    }

    protected willSendRequest(request) {
        request.headers.set("Authorization", TOKEN_HEADER)
        request.headers.set("Content-Type", "application/json")
    }

    uploadFileBase64(files): Promise<any> {
        return this.post('/upload', files)
            .catch(console.log);

    }

    listAllFilesProcessed(): Promise<any> {
        return this.get('/download/list').then(value => value)
            .catch(console.log);
    }

    downloadFileByKey(key): Promise<any> {
        return this.post('/download/getFile', key)
            .catch(console.log)
    }

}
