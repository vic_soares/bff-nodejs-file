import FileDataSource from "./file.datasource";

export default {
    Query: {
        listFiles: (root, args, {injector}) => {
            return injector.get(FileDataSource).listAllFilesProcessed();
        }
    },
    Mutation: {

        uploadBase64: (root, args, {injector}) => {
            return injector.get(FileDataSource).uploadFileBase64(args.files)
        },
        downloadFile: (root, args, {injector}) => {
            return injector.get(FileDataSource).downloadFileByKey(args.key)
        }
    }
}