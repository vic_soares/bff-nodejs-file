import {GraphQLModule} from "@graphql-modules/core";
import {commonModule} from "../common/common.module";
import FileDataSource from "./file.datasource";
import typeDefs from "./file.graphql";
import resolvers from "./file.resolver"

export const fileModule = new GraphQLModule({
    typeDefs, resolvers,
    imports: [
        commonModule
    ],
    providers:[
        FileDataSource
    ]
})