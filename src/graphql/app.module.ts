import {GraphQLModule} from '@graphql-modules/core';

import {fileModule} from "./file/file.module";
import {commonModule} from "./common/common.module";

export const appModule = new GraphQLModule({
    imports: [
        fileModule, commonModule
    ]
});

export default appModule.schema;
