import { GraphQLModule } from '@graphql-modules/core';

import typeDefs from './common.graphql';
import AuthDirective from './auth.directive'

export const commonModule = new GraphQLModule({
    typeDefs,
    schemaDirectives: {
        auth: AuthDirective
    }
  });