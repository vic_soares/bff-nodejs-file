FROM node:12-alpine as builder

WORKDIR /app

COPY . .

RUN npm ci && npm run build 


# --------------------------------------------------------------

FROM node:12-alpine

WORKDIR /app

EXPOSE 8080 8081

COPY package*.json ./
COPY .env ./

RUN npm install pm2 -g && npm install --production

COPY --from=builder /app/dist ./dist

CMD ["pm2-runtime", "dist/index.js"]